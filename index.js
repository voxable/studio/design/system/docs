const { createApp } = require('vuepress');

async function build (options) {
  const app = createApp(options);
  await app.process();
  return app.build();
}

async function dev (options) {
  const app = createApp(options);
  await app.process();
  return app.dev();
}

build({sourceDir: './', theme: '@vuepress/theme-default'}).catch(function(e) { console.error(e) });

const connect = require('connect');
const serveStatic = require('serve-static');

connect().use(serveStatic(__dirname)).listen(8080, function(){
  console.log('Server running on 8080...');
});


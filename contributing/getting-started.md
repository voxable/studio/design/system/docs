# Getting Started

Voxable Studio's design documentation makes use of the following technologies:

* [VuePress](https://vuepress.vuejs.org/) - VuePress is a static site generator for building documentation sites.
* [GitLab's Web IDE](https://gitlab.com/help/user/project/web_ide/index.html) - GitLab's Web IDE 0allows anyone to make changes to Voxable Studio's design documentation via their web browser](https://about.gitlab.com/blog/2018/06/15/introducing-gitlab-s-integrated-development-environment/), see those changes previewed live, and then commit those changes back to the project's git repository.

Review the above documentation sites to get a feel for how these technologies work, and continue to refer to them as you work on this documentation site. Most of what you may want to do should be documented by these projects already.
